package usa.kerby.tk.jchal;

import java.net.URI;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.ws.rs.core.Variant;

import usa.kerby.tk.jchal.http.HttpRequest;
import usa.kerby.tk.jchal.http.HttpResponse;
import usa.kerby.tk.jhal.Hal;
import usa.kerby.tk.jhal.HalEntity;
import usa.kerby.tk.jhal.NonProp;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.http.HttpMethod;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Jul 16, 2020
 */

@JsonbTypeSerializer(ChaloSerializer.class)
public class ChaloEntity extends HalEntity implements ChalRepresentation {

	public static class Builder extends HalEntity.Builder implements Chal {

		protected HttpResponse cacheableResponse;
		protected HttpRequest cacheableRequest;
		protected HttpMethod method = HttpMethod.GET; // Default
		// protected Locale language = Locale.US;
		// protected String encoding = StandardCharsets.UTF_8.name();
		protected Variant variant;

		public Builder(URI self, Variant variant, DataTransferObject... dto) {
			super(self, dto);
			this.variant = variant;
		}

		@Override
		public ChaloEntity.Builder addEmbedded(LinkRelation rel, Hal builder) {
			return addEmbedded(rel, builder);
		}

		@Override
		public ChaloEntity.Builder addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto) {
			return this.addEmbedded(rel, new ChaloEntity.Builder(self, this.variant, dto));
		}

		@Override
		public ChaloEntity build() {
			ChaloEntity cacheableHalo = new ChaloEntity(this);
			return cacheableHalo;
		}

		@Override
		@NonProp
		public ChaloEntity.Builder getEmbedded(LinkRelation rel, int index) {
			return (Builder) embedded.get(rel).get(index);
		}

		public Builder httpMethod(HttpMethod method) {
			this.method = method;
			return this;
		}

		public Builder variant(Variant variant) {
			this.variant = variant;
			return this;
		}

		@Override
		protected Builder self() {
			return this;
		}

		public Builder withReponse(HttpResponse cacheableReponse) {
			this.cacheableResponse = cacheableReponse;
			return this;
		}

		public Builder withRequest(HttpRequest cacheableRequest) {
			this.cacheableRequest = cacheableRequest;
			return this;
		}
	}

	@JsonbProperty("_request")
	private final HttpResponse cacheableResponse;

	@JsonbProperty("_response")
	private final HttpRequest cacheableRequest;
	private final HttpMethod method;
	private Variant variant;

	protected ChaloEntity(Builder builder) {
		super(builder);
		this.cacheableResponse = builder.cacheableResponse;
		this.cacheableRequest = builder.cacheableRequest;
		this.method = builder.method;
		this.variant = builder.variant;
	}

	/**
	 * @return the cacheableRequest
	 */
	// @NonProp
	public HttpRequest getCacheableRequest() {
		return cacheableRequest;
	}

	/**
	 * @return the cacheableResponse
	 */
	// @NonProp
	public HttpResponse getCacheableResponse() {
		return cacheableResponse;
	}

	public Variant getVariant() {
		return this.variant;
	}

	/**
	 * @return the method
	 */
	public HttpMethod getMethod() {
		return method;
	}

}
