package usa.kerby.tk.jchal;

import usa.kerby.tk.jhal.HalRepresentation;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface ChalRepresentation extends HalRepresentation {

}
