package usa.kerby.tk.jchal;

import java.net.URI;

import javax.ws.rs.core.MediaType;

import usa.kerby.tk.jhal.Hal;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface Chal extends Hal {

	public static final String APPLICATION_JSON_CHAL = "application/vnd.chal.chal+json";
	public static final MediaType APPLICATION_JSON_CHAL_TYPE = new MediaType("application", "vnd.chal.chal+json");

	public static final String APPLICATION_XML_CHAL = "application/vnd.chal.chal+json";
	public static final MediaType APPLICATION_XML_CHAL_TYPE = new MediaType("application", "vnd.chal.chal+xml");

	@Override
	public Chal addEmbedded(LinkRelation rel, Hal builder);

	@Override
	public Chal addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto);

	@Override
	public ChalRepresentation build();

	@Override
	public Chal getEmbedded(LinkRelation rel, int index);

}
