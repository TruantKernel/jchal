package usa.kerby.tk.jchal;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

import usa.kerby.tk.jchal.http.HttpRequest;
import usa.kerby.tk.jchal.http.HttpResponse;
import usa.kerby.tk.jhal.HalRepresentation;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;

/**
 * @author Trevor Kerby
 * @since Jul 20, 2020
 */
public class ChaloSerializer implements JsonbSerializer<ChaloEntity> {

	@Override
	public void serialize(ChaloEntity chalo, JsonGenerator generator, SerializationContext ctx) {
		System.out.println("chalo serializer");
		this.writeProps(chalo.getProps(), generator, ctx);
		this.writeLinks(chalo.getLinks(), generator, ctx);
		this.writeRequest(chalo.getCacheableRequest(), generator, ctx);
		this.writeResponse(chalo.getCacheableResponse(), generator, ctx);
		this.writeEmbedded(chalo.getEmbedded(), generator, ctx);

	}

	private void writeEmbedded(Map<LinkRelation, List<HalRepresentation>> map, JsonGenerator generator,
			SerializationContext ctx) {
		if (map != null && !map.isEmpty()) {
			generator.writeStartObject("_embedded");
			for (Entry<LinkRelation, List<HalRepresentation>> entry : map.entrySet()) {
				generator.writeStartArray(entry.getKey().toString());
				for (HalRepresentation child : entry.getValue()) {
					generator.writeStartObject();
					this.serialize((ChaloEntity) child, generator, ctx);
					generator.writeEnd();
				}
				generator.writeEnd();
			}
			generator.writeEnd();
		}
	}

	/**
	 * 
	 */
	private void writeLinks(Map<LinkRelation, PathDecorator<Hyperlink>> links, JsonGenerator generator,
			SerializationContext ctx) {
		if (links != null && !links.isEmpty()) {
			generator.writeStartObject("_links");
			for (Entry<LinkRelation, PathDecorator<Hyperlink>> entry : links.entrySet()) {
				PathDecorator<Hyperlink> pathedHyperlink = entry.getValue();
				ctx.serialize(entry.getKey().toString(), pathedHyperlink.getValue(), generator);
			}
			generator.writeEnd();
		}

	}

	/**
	 * @param halo
	 * @param generator
	 * @param ctx
	 */
	private void writeProps(Map<String, PathDecorator<Object>> props, JsonGenerator generator,
			SerializationContext ctx) {
		if (props != null && !props.isEmpty()) {
			for (Entry<String, PathDecorator<Object>> entry : props.entrySet()) {
				PathDecorator<Object> pathedHyperlink = entry.getValue();
				ctx.serialize(entry.getKey(), pathedHyperlink.getValue(), generator);
			}
		}

	}

	/**
	 * @param cacheableRequest
	 * @param generator
	 * @param ctx
	 */
	private void writeRequest(HttpRequest cacheableRequest, JsonGenerator generator, SerializationContext ctx) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param cacheableResponse
	 * @param generator
	 * @param ctx
	 */
	private void writeResponse(HttpResponse cacheableResponse, JsonGenerator generator, SerializationContext ctx) {
		// TODO Auto-generated method stub

	}
}