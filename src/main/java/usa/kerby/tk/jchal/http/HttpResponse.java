package usa.kerby.tk.jchal.http;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.core.Variant;

/**
 * @author Trevor Kerby
 * @since Jul 11, 2020
 */

@JsonbTypeSerializer(HttpResponseSerializer.class)
public class HttpResponse extends HttpMessage {

	public static class Builder {
		private MultivaluedMap<String, String> headers;
		private StatusType status;
		private Variant variant;
		private List<Variant> variants;
		private List<NewCookie> cookies;
		private List<Link> links;
		private CacheControl cacheControl;
		private ZonedDateTime expires;
		// ZonedDateTime.now(ZoneOffset.UTC)
		private ZonedDateTime lastModified;
		private URI uri;
		private EntityTag tag;

		public Builder(Variant variant) {
			this.variant = variant;
		}

		public Builder allow(Set<String> methods) {
			if (this.headers.containsKey("allow")) {
				List<String> header = this.headers.get("allow");
				for (String method : methods) {
					header.add(method);
				}
			} else {
				List<Object> header = new LinkedList<>();
				header.addAll(methods);
			}
			return this;
		}

		public Builder allow(String... methods) {
			return this.allow(Set.of(methods));
		}

		public HttpResponse build() {
			boolean isVaryByMediaType = false;
			boolean isVaryByLanguage = false;
			boolean isVaryByEncoding = false;
			if (this.variants != null) {
				for (Variant variant : this.variants) {
					MediaType mediaType = variant.getMediaType();
					if (mediaType != null) {
						isVaryByMediaType = true;
						this.header(HttpHeaders.CONTENT_TYPE, mediaType.toString());
					}
					Locale language = variant.getLanguage();
					if (language != null) {
						isVaryByLanguage = true;
						this.header(HttpHeaders.CONTENT_LANGUAGE, language.toString());
					}
					String enc = variant.getEncoding();
					if (enc != null) {
						isVaryByEncoding = true;
						this.header(HttpHeaders.CONTENT_ENCODING, enc);
					}
				}
				if (isVaryByMediaType) {
					this.header(HttpHeaders.VARY, HttpHeaders.ACCEPT);
				}
				if (isVaryByLanguage) {
					this.header(HttpHeaders.VARY, HttpHeaders.ACCEPT_LANGUAGE);
				}
				if (isVaryByEncoding) {
					this.header(HttpHeaders.VARY, HttpHeaders.ACCEPT_ENCODING);
				}
			}
			return new HttpResponse(this);
		}

		public Builder cacheControl(CacheControl cacheControl) {
			this.cacheControl = cacheControl;
			return this;
		}

		public Builder contentLocation(URI location) {
			this.uri = location;
			return this;
		}

		public Builder cookie(NewCookie... cookies) {
			if (this.cookies != null) {
				this.cookies = new LinkedList<>();
			}
			for (NewCookie cookie : cookies) {
				this.cookies.add(cookie);
			}
			return this;
		}

		public Builder expires(ZonedDateTime expires) {
			this.expires = expires;
			return this;
		}

		public Builder header(String name, String value) {
			if (!this.headers.containsKey(name)) {
				this.headers.put(name, new LinkedList<>());
			}
			this.headers.get(name).add(value);
			return this;
		}

		public Builder lastModified(ZonedDateTime lastModified) {
			this.lastModified = lastModified;
			return this;
		}

		public Builder link(String uri, String rel) {
			return this.link(URI.create(uri), rel);
		}

		public Builder link(URI uri, String rel) {
			return this.links(Link.fromUri(uri).rel(rel).build());
		}

		public Builder links(Link... links) {
			if (this.links != null) {
				this.links = new LinkedList<>();
			}
			for (Link link : links) {
				this.links.add(link);
			}
			return this;
		}

		public Builder location(URI location) {
			this.uri = location;
			return this;
		}

		public Builder status(Response.Status code) {
			return this.status((StatusType) code);
		}

		public Builder status(Response.StatusType code) {
			this.status = code;
			return this;
		}

		public Builder tag(EntityTag tag) {
			this.tag = tag;
			return this;
		}

		public Builder tag(String tag) {
			return this.tag(new EntityTag(tag));
		}

		public Builder variant(Variant variant) {
			this.variant = variant;
			return this;
		}

		public Builder variants(List<Variant> variants) {
			this.variants = variants;
			return this;
		}

		public Builder variants(Variant... variants) {
			return this.variants(Arrays.asList(variants));
		}

	}

	private final StatusType status;
	private final Variant variant;
	private final List<Variant> variants;
	private final List<NewCookie> cookies;
	private final List<Link> links;
	private final CacheControl cacheControl;
	private final ZonedDateTime expires;
	private final ZonedDateTime lastModified;
	private final URI uri;

	private final EntityTag tag;

	/**
	 * @param builder
	 */
	public HttpResponse(Builder builder) {
		this.headers = builder.headers;
		this.status = builder.status;
		this.variant = builder.variant;
		this.variants = builder.variants;
		this.cookies = builder.cookies;
		this.links = builder.links;
		this.cacheControl = builder.cacheControl;
		this.expires = builder.expires;
		this.lastModified = builder.lastModified;
		this.uri = builder.uri;
		this.tag = builder.tag;
	}

	public CacheControl getCacheControl() {
		return cacheControl;
	}

	public List<NewCookie> getCookies() {
		return cookies;
	}

	public ZonedDateTime getExpires() {
		return expires;
	}

	public MultivaluedMap<String, String> getHeaders() {
		return headers;
	}

	public ZonedDateTime getLastModified() {
		return lastModified;
	}

	public List<Link> getLinks() {
		return links;
	}

	public StatusType getStatus() {
		return status;
	}

	public EntityTag getTag() {
		return tag;
	}

	public URI getUri() {
		return uri;
	}

	public Variant getVariant() {
		return variant;
	}

	public List<Variant> getVariants() {
		return variants;
	}
}
