package usa.kerby.tk.jchal.http;

import java.net.URI;
import java.util.LinkedList;

import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.ws.rs.core.MultivaluedMap;

import usa.kerby.tk.jhateoas.http.HttpMethod;

/**
 * @author Trevor Kerby
 * @since Jul 11, 2020
 */

@JsonbTypeSerializer(HttpRequestSerializer.class)
public class HttpRequest extends HttpMessage {

	private HttpMethod method;
	private URI uri;

	public static class Builder {
		private MultivaluedMap<String, String> headers;
		private HttpMethod method;
		private URI uri;

		public Builder(URI uri, HttpMethod method) {
			this.uri = uri;
			this.method = method;
		}

		public Builder header(String name, String value) {
			if (!this.headers.containsKey(name)) {
				this.headers.put(name, new LinkedList<>());
			}
			this.headers.get(name).add(value);
			return this;
		}

		public HttpRequest build() {
			return new HttpRequest(this);
		}

	}

	public HttpRequest(Builder builder) {
		this.uri = builder.uri;
		this.headers = builder.headers;
		this.method = builder.method;
	}

	public HttpMethod getMethod() {
		return method;
	}

	public URI getUri() {
		return this.uri;
	}

}
