package usa.kerby.tk.jchal.http;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public abstract class HttpMessageSerializer {
	
	protected final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O", Locale.ENGLISH);

	public void serializeHeaders(HttpMessage obj, JsonGenerator generator, SerializationContext ctx) {
		//generator.writeStartObject();//"headers");
		if (obj.headers != null) {
			for (Entry<String, List<String>> entry : obj.headers.entrySet()) {
				final List<String> header = entry.getValue();
				if (header != null && !header.isEmpty()) {
					if (header.size() > 1) {
						generator.writeStartObject(entry.getKey());
						String.join("; ", entry.getValue());
						generator.writeEnd();
					} else {
						generator.write(entry.getKey(), header.get(0));
					}
				}
			}
		}
		//generator.writeEnd();
	}
}
