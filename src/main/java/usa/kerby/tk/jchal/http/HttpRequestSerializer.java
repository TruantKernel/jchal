package usa.kerby.tk.jchal.http;

import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

import usa.kerby.tk.jhal.HalSerializer;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */

@JsonbTypeSerializer(HalSerializer.class)
public class HttpRequestSerializer extends HttpMessageSerializer implements JsonbSerializer<HttpRequest> {

	@Override
	public void serialize(HttpRequest obj, JsonGenerator generator, SerializationContext ctx) {
		generator.writeStartObject("headers");
		this.serializeHeaders(obj, generator, ctx);
		generator.writeEnd();
	}

}
