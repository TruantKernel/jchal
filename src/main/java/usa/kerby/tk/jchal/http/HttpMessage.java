package usa.kerby.tk.jchal.http;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * @author Trevor Kerby
 * @since Jul 27, 2020
 */
public abstract class HttpMessage {
	protected MultivaluedMap<String, String> headers;

	/**
	 * 
	 */
	public HttpMessage() {
		this.headers = new MultivaluedHashMap<>();
	}

	public void getHeader(String header) {
		this.getHeader(header, 0);
	}

	public Object getHeader(String header, int index) {
		return this.headers.get(header).get(index);
	}

	public MultivaluedMap<String, String> getHeaders() {
		return this.headers;
	}

}
