package usa.kerby.tk.jchal.http;

import java.util.ArrayList;

import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */

public class HttpResponseSerializer extends HttpMessageSerializer implements JsonbSerializer<HttpResponse> {

	// private final StatusType status; +
	// private final Variant variant; +
	// private final List<Variant> variants; +
	// private final List<NewCookie> cookies; +
	// private final List<Link> links;
	// private final CacheControl cacheControl;
	// private final Date expires;
	// private final Date lastModified;
	// private final URI uri;
	// private final EntityTag tag;

	@Override
	public void serialize(HttpResponse obj, JsonGenerator generator, SerializationContext ctx) {
		if (obj.getStatus() != null) {
			generator.write("status", ((Response.Status) obj.getStatus()).toString());
		}
		generator.writeStartObject("headers");
		generator.write(HttpHeaders.CONTENT_TYPE, obj.getVariant().getMediaType().getType());
		generator.write(HttpHeaders.CONTENT_LANGUAGE, obj.getVariant().getLanguageString());
		generator.write(HttpHeaders.CONTENT_ENCODING, obj.getVariant().getEncoding());
		if (obj.getExpires() != null) {
			generator.write(HttpHeaders.EXPIRES, formatter.format(obj.getExpires()));
		}
		if (obj.getLastModified() != null) {
			generator.write(HttpHeaders.LAST_MODIFIED, formatter.format(obj.getLastModified()));
		}
		this.serializeCookie(obj, generator);
		this.serializeHeaders(obj, generator, ctx);
		generator.writeEnd();
	}

	public void serializeCookie(HttpResponse obj, JsonGenerator generator) {
		if (obj.getCookies() != null && obj.getCookies().size() >= 1) {
			ArrayList<String> cookieVals = new ArrayList<>(obj.getCookies().size());
			for (NewCookie cookie : obj.getCookies()) {
				cookieVals.add(cookie.getName() + "=" + cookie.getValue());
			}
			final String delimited = String.join("; ", cookieVals);
			generator.write(HttpHeaders.SET_COOKIE, delimited);
		}
	}

	public void serializeLinks(HttpResponse obj, JsonGenerator generator, SerializationContext ctx) {
		if (obj.getLinks() != null && obj.getLinks().size() >= 1) {
			for (Link link : obj.getLinks()) {
				ctx.serialize(link, generator);
			}
		}
	}

}
